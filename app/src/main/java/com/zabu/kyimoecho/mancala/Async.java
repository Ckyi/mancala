package com.zabu.kyimoecho.mancala;

import android.os.AsyncTask;

/**
 * Created by kyimoecho on 16-03-10.
 */
public class Async extends AsyncTask<Object, Object, Object> {

    Player currentPlayer;
    Player opponent;

    @Override
    protected Object doInBackground(Object... params) {
        currentPlayer = (Player)params[0];
        opponent = (Player)params[1];
        Integer grabStonefromPit = (Integer)params[2];
        Integer direction = (Integer)params[3];


        currentPlayer.makeMove(grabStonefromPit,direction);

        return null;
    }

    @Override
    protected void onPostExecute(Object result) {

        currentPlayer.onMoveFinished();


        if(currentPlayer.getGame().isGameOver())
            return;

        /*if(currentPlayer.getType() == Constants.human){
            if ((opponent.getType() != Constants.human)) {
                if(currentPlayer.isGainTurn())
                    new Async().execute(currentPlayer,opponent,-1,0);
                else
                    new Async().execute(opponent,currentPlayer,-1,0);
            }
        }*/

        if((currentPlayer.getType() == Constants.human) && (!currentPlayer.isGainTurn())){
            new Async().execute(opponent,currentPlayer,-1,0);
        }
        else if((currentPlayer.getType() != Constants.human && opponent.getType() != Constants.human)
        || (currentPlayer.isGainTurn() && currentPlayer.getType() != Constants.human)){
            if(currentPlayer.isGainTurn())
                new Async().execute(currentPlayer,opponent,-1,0);
            else
                new Async().execute(opponent,currentPlayer,-1,0);
        }



    }


}
