package com.zabu.kyimoecho.mancala;




import java.util.ArrayList;
import android.util.Log;

/**
 * Created by kyimoecho on 16-03-04.
 */

public class Game {

    public Player teamRed;
    public Player teamBlue;
    public boolean validMove;
    private boolean gainAnotherTurn;
    public Integer winner;

    public Game(Player red, Player blue)
    {
        teamRed = red;
        teamBlue = blue;
        winner = null;
        gainAnotherTurn = false;
        validMove = false;
    }


    public boolean isGameOver()
    {
        Integer player1Score = 0;
        Integer player2Score = 0;
        ArrayList<Integer> player1 = teamRed.getPitArray();
        ArrayList<Integer> player2 = teamBlue.getPitArray();


        for(int i=1; i < player1.size()-1;i++)
            player1Score += player1.get(i);

        for(int i=1; i < player2.size()-1;i++)
            player2Score += player2.get(i);

        if(player1Score != 0 && player2Score != 0)
            return false;

        player1Score += teamRed.getPitArray().get(0) + teamRed.getPitArray().get(Constants.boardWidth-1);
        player2Score += teamBlue.getPitArray().get(0) + teamBlue.getPitArray().get(Constants.boardWidth-1);

        if(player1Score > player2Score)
            winner = Constants.red;
        else if(player1Score < player2Score)
            winner = Constants.blue;
        else
            winner = Constants.blue + Constants.red;

        return true;

    }

    public boolean move(Player currentPlayer, Integer pitIndex, Integer direction)
    {
        ArrayList<Integer> playerPitArray = currentPlayer.getPitArray();
        Integer stoneCount = playerPitArray.get(pitIndex);
        Integer currentPitStoneCount;
        Player otherPlayer = (currentPlayer == teamRed ? teamBlue : teamRed);

        if(stoneCount == 0) return validMove;

        validMove = true;
        playerPitArray.set(pitIndex,0); //Take out stones from the pit

        //Redestribute them in other pits

        while(stoneCount > 0){
            pitIndex += direction;
            currentPitStoneCount = playerPitArray.get(pitIndex);
            playerPitArray.set(pitIndex,currentPitStoneCount+1);
            stoneCount--;

            if(pitIndex == 0 || pitIndex == Constants.boardWidth-1) {
                //currentTurn.setPitArray(pitArray); //Update pitarray of current side. May not need this if it returns by reference
                playerPitArray = otherPlayer.getPitArray();
                direction = (direction == Constants.forward ?
                        Constants.backward : Constants.forward);

            }

        }


        if (isGameOver()) return validMove;
        else if((pitIndex == 0) || (pitIndex == Constants.boardWidth-1)) { //Extra turn gain scenario
            gainAnotherTurn = true;
        }
        else {
            gainAnotherTurn = false;
            currentPitStoneCount = playerPitArray.get(pitIndex);
            if((currentPitStoneCount == 1) &&
               (currentPlayer.getPitArray() == playerPitArray)) //End in other side
            {
                playerPitArray = otherPlayer.getPitArray();
                Integer stones = playerPitArray.get(pitIndex);
                playerPitArray.set(pitIndex,0);
                stones += currentPlayer.getPitArray().get(Constants.boardWidth-1);
                currentPlayer.getPitArray().set(Constants.boardWidth - 1, stones);
                System.out.print(currentPlayer.getPitArray().set(Constants.boardWidth - 1, stones));
            }

        }

        return validMove;
    }

    public void stateToGame(State state)
    {
        ArrayList<Integer> oldStatePitArray1 = teamRed.getPitArray();
        ArrayList<Integer> oldStatePitArray2 = teamBlue.getPitArray();
        ArrayList<Integer> newStatePitArray1 = state.getRedArray();
        ArrayList<Integer> newStatePitArray2 = state.getBlueArray();

        for (int i = 0; i < oldStatePitArray1.size(); i++) {
            oldStatePitArray1.set(i, newStatePitArray1.get(i));
            oldStatePitArray2.set(i, newStatePitArray2.get(i));
        }


    }

    public boolean hypotheticalMove(ArrayList<Integer> playerPitArray, ArrayList<Integer> opponentPitArray,
                                 Integer pitIndex, Integer direction)
    {
        Integer stoneCount = playerPitArray.get(pitIndex);
        Integer currentPitStoneCount;
        //Player otherPlayer = (currentPlayer == teamRed ? teamBlue : teamRed);

        if(stoneCount == 0) return false;

        validMove = true;


        playerPitArray.set(pitIndex,0); //Take out stones from the pit

        //Redestribute them in other pits


        while(stoneCount > 0){
            pitIndex += direction;
            currentPitStoneCount = playerPitArray.get(pitIndex);
            playerPitArray.set(pitIndex,currentPitStoneCount+1);
            stoneCount--;

            if(pitIndex == 0 || pitIndex == Constants.boardWidth-1) {
                //playerPitArray = otherPlayer.getPitArray();
                playerPitArray = opponentPitArray;
                direction = (direction == Constants.forward ?
                        Constants.backward : Constants.forward);

            }

        }

        if((pitIndex == 0) || (pitIndex == Constants.boardWidth-1)) {
            gainAnotherTurn = true;
        }
        else {
            gainAnotherTurn = false;
            currentPitStoneCount = playerPitArray.get(pitIndex);
            if((currentPitStoneCount == 1) &&
                    (opponentPitArray != playerPitArray)) //End in other side
            {
                /*playerPitArray = otherPlayer.getPitArray();
                Integer stones = playerPitArray.get(pitIndex);
                playerPitArray.set(pitIndex,0);
                stones += currentPlayer.getPitArray().get(Constants.boardWidth-1);
                currentPlayer.getPitArray().set(Constants.boardWidth - 1, stones);
                System.out.print(currentPlayer.getPitArray().set(Constants.boardWidth - 1, stones));*/

                Integer stones = opponentPitArray.get(pitIndex);
                opponentPitArray.set(pitIndex,0);
                stones += playerPitArray.get(Constants.boardWidth-1);
                playerPitArray.set(Constants.boardWidth - 1, stones + 1); //Total stones plus the last stone you drop
                playerPitArray.set(pitIndex, 0);
                //System.out.print(currentPlayer.getPitArray().set(Constants.boardWidth - 1, stones));

            }

        }

        return validMove;
    }


    public boolean isGainAnotherTurn()
    {
        return gainAnotherTurn;
    }

}

