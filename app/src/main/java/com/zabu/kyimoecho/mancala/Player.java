package com.zabu.kyimoecho.mancala;

import java.util.ArrayList;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by kyimoecho on 16-03-04.
 */


public class Player{

    private ArrayList<Integer> pitStoneCount;
    private Game game;
    private Integer type;
    private MainActivity mainActivity;
    private boolean validMove = false;
    private Player opponent;
    private Integer color;
    private boolean extraTurn;
    private Integer nodeCount;

    public boolean isGainTurn()
    {
        return extraTurn;
    }

    public Player(MainActivity argMainActivity, Integer c)
    {
        pitStoneCount = new ArrayList<>();

        //Set stonecount of macala to zeros

        for(int i = 0; i < Constants.boardWidth; i++){
            if((i == 0) ||  (i == Constants.boardWidth-1)) pitStoneCount.add(i, 0);
            else pitStoneCount.add(i,Constants.stones);
        }

        this.mainActivity = argMainActivity;
        this.color = c;
        this.extraTurn = false;
        this.nodeCount = 0;
    }

    public Game getGame() {return game;}


    public Integer getType()
    {
        return type;
    }

    public void setGame(Game argGame)
    {
       this.game = argGame;
    }

    public void setOpponent(Player opponent)
    {
        this.opponent = opponent;
    }

    public void setType(Integer argType)
    {
        this.type = argType;
    }

    public ArrayList<Integer> getPitArray()
    {
        return pitStoneCount;
    }

    public void setPitArray(ArrayList<Integer> stoneList)
    {
        this.pitStoneCount = stoneList;
    }

    public boolean makeMove(Integer grabStonefromPit, Integer direction) {

        if(type == Constants.human)
        {
            validMove = game.move(this,grabStonefromPit,direction);
            extraTurn = game.isGainAnotherTurn();

        }
        else
        {
            State currentState;
            State nextState;

            if(color == Constants.red) {
                currentState = new State(pitStoneCount, opponent.getPitArray(), Constants.heuristic_1,color);
                nextState = miniMax(currentState, 3, true, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
                game.stateToGame(nextState);
                extraTurn = nextState.isExtraTurn();
                Log.d("Red Player Node #:", nodeCount.toString());
                nodeCount = 0;
            }
            else{
               // currentState = new State(opponent.getPitArray(), pitStoneCount, Constants.heuristic_1,color);
                currentState = new State(opponent.getPitArray(), pitStoneCount, Constants.heuristic_2,color);
                nextState = miniMax(currentState, 3, true, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
                game.stateToGame(nextState);
                extraTurn = nextState.isExtraTurn();
                Log.d("Blue Player Node #:", nodeCount.toString());
                nodeCount = 0;
            }

            validMove = true;

        }

        return validMove;
    }



    public State miniMax(State state, Integer depth, boolean maxPlayer, Double alpha, Double beta)
    {
        if (state.isGameOver() || depth == 0) {
            // if this is opponent return neg score. otherwise big score
            return state;
        }

        if (state.isExtraTurn()){
            return state;
        }

        ArrayList<State> nextMoves = generateValidMoves(state,state.getWhosTurn());
        State newState;


        for(State s : nextMoves){
            if(maxPlayer)
            {
                newState = miniMax(s,depth-1,false,alpha,beta);
                if(newState.getScore() > alpha)
                {
                    alpha = newState.getScore();
                    state = newState;

                }
                nodeCount++;
            }
            else
            {
                newState = miniMax(s,depth-1,true,alpha,beta);
                if(newState.getScore() < beta)
                {
                    beta = newState.getScore();
                }
                nodeCount++;
            }
            if(alpha >= beta) break;
        }

        return state;

    }


    private ArrayList<State> generateValidMoves(State state, Integer whosTurn)
    {
        ArrayList<State> stateArray = new ArrayList<>();

        for(int i=1;i < Constants.boardWidth-1; i++) {
            //Forward
            ArrayList<Integer> me = new ArrayList<>();
            ArrayList<Integer> myOpponent = new ArrayList<>();
            ArrayList<Integer> me2 = new ArrayList<>();
            ArrayList<Integer> myOpponent2 = new ArrayList<>();

            if(whosTurn == Constants.red) {
            //if(color == Constants.red) {
                me.addAll(state.getRedArray());
                myOpponent.addAll(state.getBlueArray());

                me2.addAll(state.getRedArray());
                myOpponent2.addAll(state.getBlueArray());

                boolean valid = game.hypotheticalMove(me, myOpponent, i, Constants.forward);
                Integer turn = (game.isGainAnotherTurn() ? Constants.red : Constants.blue);
                if (valid){
                    State newState1 = new State(me, myOpponent, Constants.heuristic_1,turn);
                    newState1.setExtraTurn(game.isGainAnotherTurn());
                    stateArray.add(newState1);
                }

                boolean valid2 = game.hypotheticalMove(me2, myOpponent2, i, Constants.backward);
                turn = (game.isGainAnotherTurn() ? Constants.red : Constants.blue);
                if (valid2){
                    State newState2 =new State(me2, myOpponent2, Constants.heuristic_1,turn);
                    newState2.setExtraTurn(game.isGainAnotherTurn());
                    stateArray.add(newState2);
                }
            }
            else{
                me.addAll(state.getBlueArray());
                myOpponent.addAll(state.getRedArray());

                me2.addAll(state.getBlueArray());
                myOpponent2.addAll(state.getRedArray());

                boolean valid = game.hypotheticalMove(me, myOpponent, i, Constants.forward);
                Integer turn = (game.isGainAnotherTurn() ? Constants.blue:Constants.red);
                if (valid) {
                    State newState1 = new State(myOpponent, me, Constants.heuristic_2,turn);
                    newState1.setExtraTurn(game.isGainAnotherTurn());
                    stateArray.add(newState1);
                }

                boolean valid2 = game.hypotheticalMove(me2, myOpponent2, i, Constants.backward);
                turn = (game.isGainAnotherTurn() ? Constants.blue:Constants.red);
                if (valid2){
                    State newState2 = new State(myOpponent2, me2,Constants.heuristic_2,turn);
                    newState2.setExtraTurn(game.isGainAnotherTurn());
                    stateArray.add(newState2);
                }
            }

        }

        return stateArray;


    }


    /*@Override
    protected void onProgressUpdate(Integer... values) {
        mainActivity.updateUI();
        mainActivity.enableDisablePlayer(opponent,true);

        if((type == Constants.human) || (opponent.getType() != Constants.human)) {
            opponent.execute(0);
        }


    }*/


    public void onMoveFinished() {

        mainActivity.updateUI();
        if(extraTurn && type == Constants.human) {
            mainActivity.enableDisablePlayer(opponent, false);
            mainActivity.enableDisablePlayer(this, true);
        }
        else{
            mainActivity.enableDisablePlayer(opponent, true);
            mainActivity.enableDisablePlayer(this, false);
        }
        if(game.isGameOver()) {
            if(game.winner == Constants.red)
                Toast.makeText(mainActivity, "Red Won!", Toast.LENGTH_LONG).show();
            else if(game.winner == Constants.blue)
                Toast.makeText(mainActivity, "Blue Won!", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(mainActivity, "Draw!", Toast.LENGTH_LONG).show();
        }
            /*if((type == Constants.human) || (opponent.getType() != Constants.human)) {
                opponent.execute(0);
            }*/

        //}


    }
}
