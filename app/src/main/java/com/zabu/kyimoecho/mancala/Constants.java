package com.zabu.kyimoecho.mancala;

/**
 * Created by kyimoecho on 16-03-04.
 */
public class Constants {
    static final Integer boardWidth = 8;
    static final Integer forward = 1;
    static final Integer backward = -1;
    static final Integer stones = 6;
    static final String playerVsComputer="Player Vs Computer";
    static final String computerVsComputer="Computer Vs Computer";
    static final String start="Start";
    static final String restart="Restart";
    static final String humanTurnStr="Your turn";
    static final String computerTurnStr="Computer turn";
    static final Integer humanPlayerTurn = 1;
    static final Integer computer1Turn = 2;
    static final Integer computer2Turn = 3;
    static final Integer human = 1;
    static final Integer computer = 2;
    static final String computer1TurnStr= "Computer One Turn";
    static final String computer2TurnStr = "Computer Two Turn";
    static final String readyStr = "Ready!";
    static final String notPickFromMacala = "You cannot grab stones from Macala";
    static final Integer win = 1;
    static final Integer lose = -1;
    static final Integer draw = 0;
    static final Integer heuristic_1 = 1;
    static final Integer heuristic_2 = 2;
    static final Integer red = 1;
    static final Integer blue = 2
            ;


}
