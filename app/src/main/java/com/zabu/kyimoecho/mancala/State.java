package com.zabu.kyimoecho.mancala;

import java.util.ArrayList;
/**
 * Created by kyimoecho on 16-03-06.
 */
public class State {
    private ArrayList<Integer> red;
    private ArrayList<Integer> blue;
    private Double score;
    private Integer redMancala;
    private Integer blueMancala;
    private Integer redRockCount;
    private Integer blueRockCount;
    private boolean gameOver;
    private Integer whosTurn;
    private boolean extraTurn;


    public State(ArrayList<Integer> r, ArrayList<Integer> b, Integer heuristic, Integer turn)
    {
        red = new ArrayList<>();
        blue = new ArrayList<>();
        redMancala = 0;
        blueMancala = 0;
        redRockCount = 0;
        blueRockCount = 0;
        score = 0.0;
        gameOver = false;

        for(int i = 0; i < r.size(); i++) {
            if(i == 0 || i == r.size()-1) redMancala += r.get(i);
            else redRockCount += r.get(i);
            red.add(r.get(i));
        }

        for(int i = 0; i < b.size(); i++) {
            if(i == 0 || i == b.size()-1) blueMancala += b.get(i);
            else blueRockCount += b.get(i);
            blue.add(b.get(i));
        }

        whosTurn = turn;

        if(heuristic == Constants.heuristic_1)
            computeScoreWithHeuristicOne();
        else
            computeScoreWithHeuristicTwo();

        extraTurn = false;

        //Check if it is gameover state
        if(redRockCount == 0 || blueRockCount == 0) gameOver = true;

    }

    public boolean isExtraTurn()
    {
        return extraTurn;
    }

    public void setExtraTurn(boolean e)
    {
        extraTurn = e;
    }


    public Integer getWhosTurn()
    {
       return whosTurn;
    }

    public ArrayList<Integer> getRedArray() {return red;}

    public ArrayList<Integer> getBlueArray() {return blue;}

    public Double getScore() {return score;}

    public boolean isGameOver() {
        return gameOver;
    }

    private void computeScoreWithHeuristicOne()
    {
        if(whosTurn == Constants.red) {
            //score = (double) ((blueRockCount - redRockCount) + (blueMancala - redMancala));
            //score = (double) (blueRockCount - redRockCount);
            score = (double) ((blueRockCount + blueMancala) - (redRockCount + redMancala));
        }
        else
        {
            //score = (double) (redRockCount + redMancala - blueRockCount);
            score = (double) ((redRockCount + redMancala ) - (blueRockCount + blueMancala));
        }


    }

    private void computeScoreWithHeuristicTwo()
    {
        if(whosTurn == Constants.red) {
            //score = (double) ((blueRockCount + blueMancala) - (redRockCount + redMancala));
            //score = (double) (blueRockCount + blueMancala - redRockCount);
            score = (double) (redRockCount + redMancala - blueRockCount);
            score *= -1;
        }
        else
        {
            //score = (double) (redRockCount + redMancala - blueRockCount);
            score = (double) (blueRockCount + blueMancala - redRockCount);
            score *= -1;
        }

    }
}
