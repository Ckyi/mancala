package com.zabu.kyimoecho.mancala;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;
/**
 * Created by kyimoecho on 16-03-04.
 */
public class GameTest extends ApplicationTestCase<Application> {
    public GameTest() {
        super(Application.class);
    }

    public void testMoveNormal() throws Exception {
        Player red = new Player(null,Constants.red);
        Player blue = new Player(null,Constants.blue);

        Game game = new Game(red,blue);

        red.setGame(game);
        blue.setGame(game);

        game.move(red, 2, Constants.forward);

        assertEquals((int) red.getPitArray().get(2), 0);
        assertEquals((int)red.getPitArray().get(3), 7);
        assertEquals((int)red.getPitArray().get(4), 7);
        assertEquals((int)red.getPitArray().get(5), 7);
        assertEquals((int)red.getPitArray().get(6), 7);
        assertEquals((int)red.getPitArray().get(7), 1);
        assertEquals((int)blue.getPitArray().get(6), 7);


    }

    public void testMoveCapture() throws Exception {
        Player red = new Player(null,Constants.red);
        Player blue = new Player(null,Constants.blue);

        Game game = new Game(red,blue);

        red.setGame(game);
        blue.setGame(game);


        red.getPitArray().set(6, 0);
        red.getPitArray().set(1, 5);
        game.move(red, 1, Constants.forward);

        assertEquals((int) red.getPitArray().get(1), 0);
        assertEquals((int)red.getPitArray().get(2), 7);
        assertEquals((int)red.getPitArray().get(3), 7);
        assertEquals((int)red.getPitArray().get(4), 7);
        assertEquals((int)red.getPitArray().get(5), 7);
        assertEquals((int)red.getPitArray().get(6), 1);
        assertEquals((int)red.getPitArray().get(7), 6);
        assertEquals((int)blue.getPitArray().get(6), 0);

    }

    public void testMiniMax() throws Exception {
        Player red = new Player(null,Constants.red);
        Player blue = new Player(null,Constants.blue);

        Game game = new Game(red,blue);

        red.setGame(game);
        blue.setGame(game);


        State state1 = new State(red.getPitArray(),blue.getPitArray(),Constants.heuristic_1,Constants.red);
        state1 = red.miniMax(state1,3,true,Double.NEGATIVE_INFINITY,Double.POSITIVE_INFINITY);

        /*for(int i=0; i < state1.getRedArray().size(); i++)
            Log.d("RED",state1.getRedArray().get(i).toString());

        for(int i=0; i < state1.getBlueArray().size(); i++)
            Log.d("BLUE",state1.getBlueArray().get(i).toString());
*/
        assertEquals(1,1);

    }
}
